include(FindPackageHandleStandardArgs)


find_path(SILO_INCLUDE_DIR
          NAMES silo.h
          PATHS /usr/include /usr/local/include)

find_library(SILO_LIBRARY
             NAMES silo siloh5
             PATHS /usr/lib /usr/local/lib)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(SILO DEFAULT_MSG SILO_LIBRARY SILO_INCLUDE_DIR)
