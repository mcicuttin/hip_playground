cmake_minimum_required(VERSION 3.18)
include(CheckLanguage)

if(NOT DEFINED HIP_PATH)
    if(NOT DEFINED ENV{HIP_PATH})
        set(HIP_PATH "/opt/rocm/hip" CACHE PATH "Path to which HIP has been installed")
    else()
        set(HIP_PATH $ENV{HIP_PATH} CACHE PATH "Path to which HIP has been installed")
    endif()
endif()
set(CMAKE_MODULE_PATH "${HIP_PATH}/cmake" ${CMAKE_MODULE_PATH})
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

project(hip_test)

find_package(HIP MODULE REQUIRED)

set(CMAKE_HIP_LINK_EXECUTABLE "${HIP_HIPCC_CMAKE_LINKER_HELPER} ${HCC_HOME} <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
if (DEFINED ENV{HIP_PLATFORM}) 
    if ($ENV{HIP_PLATFORM} STREQUAL "nvcc")
        set(CMAKE_HIP_LINK_EXECUTABLE "/usr/local/cuda/bin/nvcc <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
    endif()
endif()

find_package(SILO)
if (SILO_FOUND)
    add_definitions(-DHAVE_SILO)
    include_directories("${SILO_INCLUDE_DIR}")
    set(LINK_LIBS ${LINK_LIBS} ${SILO_LIBRARY})
endif()

set_source_files_properties(hip_helloworld.cpp PROPERTIES HIP_SOURCE_PROPERTY_FORMAT 1)
set_source_files_properties(hip_wave.cpp PROPERTIES HIP_SOURCE_PROPERTY_FORMAT 1)


hip_add_executable(hip_helloworld hip_helloworld.cpp)

hip_add_executable(hip_wave hip_wave.cpp)
target_link_libraries(hip_wave ${LINK_LIBS})



