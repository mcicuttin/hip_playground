To compile with NVidia GPUs:

```
export HIP_PLATFORM=nvcc
export HIP_COMPILER=nvcc
```

delete `CMakeCache.txt` and recompile.

To debug: `HIPCC_VERBOSE=7`

Assumes CUDA in PATH.
