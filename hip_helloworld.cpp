#include <iostream>

#include "hip/hip_runtime.h"

#define CHECK(x) { 				\
	assert(x == hipSuccess);	\
}

int main(void)
{
 	hipDeviceProp_t devProp;
 	CHECK( hipGetDeviceProperties(&devProp, 0) );

 	std::cout << "Device string: " << devProp.name << std::endl;

    return 0;
}
