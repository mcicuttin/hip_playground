#include <iostream>
#include <cassert>
#include <sstream>

#include "fd_grid.hpp"
#include "fd_wave.hpp"

#ifdef HAVE_SILO
#include "fd_dataio.hpp"
#endif

#include "hip/hip_runtime.h"

/* :0:rocdevice.cpp :2180: 12251027947 us: Device::callbackQueue aborting with status: 0x100f
 * Does it happen with too many threads in a threadblock?
 */


#define CHECK(x) { 				\
	assert(x == hipSuccess);	\
}

/* 8th order 2nd derivative weights */
__constant__ float  d_w32[9];
__constant__ double d_w64[9];

#define WAVE_8_KER_ROWS           24
#define WAVE_8_KER_COLS           32

template<typename T>
__global__ void
wave_2D_kernel_hip(T* u_prev, T* u_curr, T* u_next, wave_2D_params<T> params)
{
    //     HHHH
    //    H****H     H = halo, * = data
    //    H****H     We load a tile as in figure. This wastes bandwidth
    //    H****H     because halos are loaded two times each, but it is
    //    H****H     the easiest way to remain generic w.r.t. grid size
    //     HHHH

#ifdef USE_SHARED_PREV
    __shared__ T s_prev[WAVE_8_KER_ROWS][WAVE_8_KER_COLS];
#endif
    __shared__ T s_curr[WAVE_8_KER_ROWS+2*WAVE_8_HALO_SIZE][WAVE_8_KER_COLS+2*WAVE_8_HALO_SIZE];

    int maxrow  = params.maxrow;
    int maxcol  = params.maxcol;
    T   dt      = params.dt;
    T   c       = params.velocity;
    T   a       = params.damping;

    assert(maxcol > 1);
    assert(maxrow > 1);

    T c2dt2 = c*c*dt*dt;
    T kx2 = c2dt2 * (maxcol-1)*(maxcol-1);
    T ky2 = c2dt2 * (maxrow-1)*(maxrow-1);
    /* Don't ask me why but if you put the rhs of this in the computation
     * of val the kernel runs 2x slower */
    T one_minus_adt = (1.0 - a*dt);
    T two_minus_adt = (2.0 - a*dt);

    /* Get X/Y coordinates of this grid point */
    int ofs_x = hipBlockDim_x * hipBlockIdx_x + hipThreadIdx_x + WAVE_8_HALO_SIZE;
    int ofs_y = hipBlockDim_y * hipBlockIdx_y + hipThreadIdx_y + WAVE_8_HALO_SIZE;

    /* Get offset of this grid point in the fd grid array */
    int ofs = (maxcol + 2*WAVE_8_HALO_SIZE) * ofs_y + ofs_x;

    if ( (ofs_x < (maxcol + WAVE_8_HALO_SIZE)) and (ofs_y < (maxrow + WAVE_8_HALO_SIZE)) )
    {
        int i = hipThreadIdx_y + WAVE_8_HALO_SIZE;
        int j = hipThreadIdx_x + WAVE_8_HALO_SIZE;

        s_curr[i][j] = u_curr[ofs];

#ifdef USE_SHARED_PREV
        s_prev[hipThreadIdx_y][hipThreadIdx_x] = u_prev[ofs];
#endif
        if (hipThreadIdx_x < WAVE_8_HALO_SIZE)
        {
            /* X-halo */
            int ox = min(WAVE_8_KER_COLS, maxcol-hipBlockDim_x * hipBlockIdx_x);
            s_curr[i][j-WAVE_8_HALO_SIZE] = u_curr[ofs-WAVE_8_HALO_SIZE];
            s_curr[i][j+ox] = u_curr[ofs+ox];
        }

        if (hipThreadIdx_y < WAVE_8_HALO_SIZE)
        {
            /* Y-halo */
            int oy = min(WAVE_8_KER_ROWS, maxrow-hipBlockDim_y * hipBlockIdx_y);
            int b_ofs = ofs - WAVE_8_HALO_SIZE * (2*WAVE_8_HALO_SIZE+maxcol);
            int t_ofs = ofs + oy * (2*WAVE_8_HALO_SIZE+maxcol);
            s_curr[i-WAVE_8_HALO_SIZE][j]  = u_curr[b_ofs];
            s_curr[i+oy][j]  = u_curr[t_ofs];
        }

        __syncthreads();

        T* w;
        if ( std::is_same<T,float>::value)
            w = (T*)d_w32;
        if ( std::is_same<T,double>::value)
            w = (T*)d_w64;

        T deriv_x = 0.0;
        //deriv_x = kx2 * (s_curr[i][j-1] - 2.0*s_curr[i][j] + s_curr[i][j+1]);
        for (int k = -WAVE_8_HALO_SIZE; k <= WAVE_8_HALO_SIZE; k++)
            deriv_x += kx2 * w[k+WAVE_8_HALO_SIZE] * s_curr[i][j+k];

        T deriv_y = 0.0;
        //deriv_y = ky2 * (s_curr[i-1][j] - 2.0*s_curr[i][j] + s_curr[i+1][j]);
        for (int k = -WAVE_8_HALO_SIZE; k <= WAVE_8_HALO_SIZE; k++)
            deriv_y += ky2 * w[k+WAVE_8_HALO_SIZE] * s_curr[i+k][j];

#ifdef USE_SHARED_PREV
        T val = (deriv_x + deriv_y)
              - one_minus_adt * s_prev[hipThreadIdx_y][hipThreadIdx_x]
              + two_minus_adt * s_curr[i][j];
#else 
        T val = (deriv_x + deriv_y)
              - one_minus_adt * u_prev[ofs]
              + two_minus_adt * s_curr[i][j];
#endif

        if ( (ofs_x == WAVE_8_HALO_SIZE) or (ofs_y == WAVE_8_HALO_SIZE) or
             (ofs_x == maxcol+WAVE_8_HALO_SIZE-1) or (ofs_y == maxrow+WAVE_8_HALO_SIZE-1)
           )
            val = 0;

        u_next[ofs] = val;
    }
}

template<typename T>
struct gpu_pointers {    
    T *         d_prev;
    T *         d_curr;
    T *         d_next;
    
    void swap(void)
    {
        T *temp;
        
        temp = d_prev;
        d_prev = d_curr;
        d_curr = d_next;
        d_next = temp;  
    }
};

template<typename T>
void initialize(wave_equation_context<T>& wec, gpu_pointers<T>& gp)
{
    static_assert(std::is_same<T,float>::value ||
                  std::is_same<T,double>::value, "Only float or double");
    /**** Initialize constants ****/
    T w0 = -205.0/72.0;
    T w1 = 8.0/5.0;
    T w2 = -1.0/5.0;
    T w3 = 8.0/315.0;
    T w4 = -1.0/560.0;
    T w[9] = { w4, w3, w2, w1, w0, w1, w2, w3, w4 };

    if (std::is_same<T,float>::value)
        CHECK( hipMemcpyToSymbol(d_w32, w, 9*sizeof(T), 0, hipMemcpyHostToDevice) );
    if (std::is_same<T,double>::value)
        CHECK( hipMemcpyToSymbol(d_w64, w, 9*sizeof(T), 0, hipMemcpyHostToDevice) );

    CHECK( hipMalloc((void**)&gp.d_prev, wec.g_prev.size()*sizeof(T)) );
    CHECK( hipMalloc((void**)&gp.d_curr, wec.g_curr.size()*sizeof(T)) );
    CHECK( hipMalloc((void**)&gp.d_next, wec.g_next.size()*sizeof(T)) );

    /* Copy grids on device */
    CHECK( hipMemcpy(gp.d_prev, wec.g_prev.data(), wec.g_prev.size()*sizeof(T), 
                     hipMemcpyHostToDevice) );
    CHECK( hipMemcpy(gp.d_curr, wec.g_curr.data(), wec.g_curr.size()*sizeof(T), 
                     hipMemcpyHostToDevice) );
    CHECK( hipMemcpy(gp.d_next, wec.g_next.data(), wec.g_next.size()*sizeof(T),  
                     hipMemcpyHostToDevice) );
}

template<typename T>
void run_timestepping(wave_equation_context<T>& wec, gpu_pointers<T>& gp)
{
    /* Simulation parameters */
    wave_2D_params<T> params;
    params.maxcol = wec.g_curr.domain_cols();
    params.maxrow = wec.g_curr.domain_rows();
    params.dt = wec.dt;
    params.velocity = wec.velocity;
    params.damping = wec.damping;
    
    /* Launch parameters */
    dim3 grid_size(wec.g_curr.domain_cols()/WAVE_8_KER_COLS + 1,
                   wec.g_curr.domain_rows()/WAVE_8_KER_ROWS + 1);

    dim3 threads_per_block(WAVE_8_KER_COLS, WAVE_8_KER_ROWS);
    
    for (size_t i = 0; i < wec.maxiter; i++)
    {
        //wave_2D_kernel_hip<<<grid_size, threads_per_block, 0, 0>>>(
        //                gp.d_prev, gp.d_curr, gp.d_next, params); 
    
        hipLaunchKernelGGL(wave_2D_kernel_hip<T>, grid_size, threads_per_block, 0, 0,
                           gp.d_prev, gp.d_curr, gp.d_next, params);
        
        //std::cout << hipGetErrorString( hipGetLastError() ) << std::endl;                   
                           
        //hipDeviceSynchronize();
        
        gp.swap();
        
#ifdef HAVE_SILO
        if ( (i%100) == 0 )
        {
            CHECK( hipMemcpy(wec.g_curr.data(), gp.d_curr, wec.g_curr.size()*sizeof(T), hipMemcpyDeviceToHost) );
            std::stringstream ss;
            ss << "wave_cuda_" << i << ".silo";
            visit_dump(wec.g_curr, ss.str());
        }
#endif /* HAVE_SILO */
    }
}

template<typename T>
void finalize(wave_equation_context<T>& wec, gpu_pointers<T>& gp)
{
    hipFree(gp.d_prev);
    hipFree(gp.d_curr);
    hipFree(gp.d_next);
}

int main(void)
{
    using T = double;

 	hipDeviceProp_t devProp;
 	CHECK( hipGetDeviceProperties(&devProp, 0) );

 	std::cout << "Device string: " << devProp.name << std::endl;

    size_t sz = 512;
    wave_equation_context<T> wec(sz, sz, 1, 0.1, 0.0001, 5000);
    gpu_pointers<T> gp;

    initialize(wec, gp);
    run_timestepping(wec, gp);
    finalize(wec, gp);

    return 0;
}

